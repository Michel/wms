module Wms
  module Dummy
    class Page < Hyperloop::Router::Component

      render do
        H1 {"I'm a dummy"}
        UL {
          Dummy.all.each do |dummy|
            LI {dummy.first}
          end
        }
      end
    end
  end
end