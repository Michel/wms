module Wms
  module Dummy
    class Engine < ::Rails::Engine
      isolate_namespace Wms::Dummy
    end
  end
end
