$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
load "wms/dummy/version.rb"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name = "wms-dummy"
  s.version = Wms::Dummy::VERSION
  s.authors = ["Michael Sprauer"]
  s.email = ["Michael.Sprauer@sap.com"]
  s.homepage = "TODO"
  s.summary = "TODO: Summary of Wms::Dummy."
  s.description = "TODO: Description of Wms::Dummy."
  s.license = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 5.2.0"

  s.add_development_dependency "sqlite3"
end
