class CreateDummy < ActiveRecord::Migration[5.2]
  def change
    create_table :dummies do |t|
      t.string :first
      t.string :second
      t.string :third

      t.timestamps
    end

    Route.create! name: "Dummy", path: "/dummy", mount: "Wms::Dummy::Page"
  end
end
