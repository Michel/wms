class Route < ApplicationRecord
  regulate_scope :all
  validates_uniqueness_of :path

  if RUBY_ENGINE == "opal"
    def component
      mount.constantize
    rescue NameError
      #`var script = document.createElement('script');
      #script.src = '/assets/components/'+#{mount.underscore.gsub("::", "/")}+'.js';
      #document.write(script.outerHTML);`
      return "Home".constantize
    end
  else
    def component
      "Home".constantize
    rescue NameError
      return "Home".constantize
    end
  end

  def opts
    return {} if mount.nil?
    opts = {mounts: component}
    opts[:exact] = "true" if path == "/"
    opts
  end
end