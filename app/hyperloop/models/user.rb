class User < ApplicationRecord
  has_secure_password unless Hyperloop.on_client?

  def self.current
    Hyperloop.on_client? ? (Hyperloop::Application.acting_user_id && find(Hyperloop::Application.acting_user_id)) : find(session[:current_user_id])
  end

  def admin?
    true
  end
end