require_relative 'application_record'
require_relative '../../models/plugin'

class Plugin < ::ApplicationRecord
  before_commit :delete_plugin_dir, on: :destroy

  regulate_scope :all
  validates_uniqueness_of :name
  validates_format_of :name, with: /wms-.*/i

  default_scope server: -> {order(:name)},
                select: -> {sort{|a,b| a.name<=>b.name}}

  def const_name
    "Wms::#{name.scan(/wms-(.*)/)[0][0].tr("-".freeze, "_".freeze).camelize}"
  end

  server_method :reload_plugin do
    PluginLoader.new(self).load!
  end

  server_method :gemspec_file do
    "#{base_dir}/#{name}.gemspec"
  end

  server_method :gemspec do
    @gemspec ||= Gem::Specification::load(gemspec_file)
  end

  server_method :description do
    begin
      gemspec.description
    rescue StandardError => e
      e.message
    end
  end

  server_method :delete_plugin_dir do
    dir = File.realpath(base_dir)
    Rails.logger.info "Deleting plugin #{name}:#{version} in '#{dir}'"
    FileUtils.remove_entry_secure dir if dir.starts_with?("#{Rails.root}/plugins")

    Rails.application.reloader.reload!
  end

  def base_dir
    "#{Rails.root}/plugins/#{name}"
  end
end
