class AppRouter < Hyperloop::Router
  history :browser
  state user: nil
  state routes: []

  before_mount do
    # any initialization particularly of state variables goes here.
    # this will execute on server (prerendering) and client.
    mutate.user User.current
    mutate.routes ::Route.all
  end

  after_mount do
    Materialize.AutoInit
  end

  route do
    if state.user
      MAIN do
        MainNavigation(routes: state.routes, on_logout: ->(user) {mutate.user nil}) {}

        DIV(class: 'container') do
          Switch do
            state.routes.each do |route|
              Route(route.path, route.opts)
            end
            Route('', exact: true) {Redirect('/', replace: true)}
          end
        end
      end
    else
      Session::LoginPage(on_login: ->(user) {mutate.user user}) {}
    end
  end
end
