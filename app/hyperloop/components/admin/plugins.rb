require 'models/plugin'

module Admin
  class Plugins < Hyperloop::Router::Component
    state plugins: []
    state loading: false
    before_mount do
      # any initialization particularly of state variables goes here.
      # this will execute on server (prerendering) and client.
      mutate.plugins ::Plugin.all
    end

    def plugin_card(plugin)
      DIV(class_name: 'col s4') do
        DIV(class_name: "card small") do
          DIV(class_name: 'card-content') do
            H3(class:"card-title") {
              SPAN{plugin.name}
              SPAN(class:"badge green white-text"){plugin.version}
            }
            P {plugin.description}
            P(class: "red-text") {plugin.load_error}
          end
          DIV(class: "card-action"){
            A(class_name: "red-text") {"uninstall"}.on(:click) {
              mutate.loading true
              plugin.destroy.then {wait_for_connection}.catch {mutate.loading false}
            }
            A(class_name: "btn btn-small waves-effect waves-light blue") {"reload"}.on(:click) {
              mutate.loading true
              plugin.reload_plugin!
              wait_for_connection
            }
          }
        end
      end
    end

    render do
      DIV(class_name: 'card') do
        DIV(class_name: 'card-content row') do
          DIV(class: "col s12 m9 l10") do
            H1 {"Plugins"}
            Loadable({loadable: state.plugins, loading: state.loading}) do
              state.plugins.each do |plugin|
                plugin_card(plugin)
              end
            end
          end
          DIV(class: "col hide-on-small-only m3 l2") do
            A(class: "btn btn-floating waves-effect right") {"Reload"}.on(:click) {
              mutate.loading true
              ::Admin::PluginReloadOp.run.then {
                # Block any activity on the website to avoid corruption of the backend, yielding
                # AccessViolations for every subsequent request.
                wait_for_connection
              }.catch {mutate.loading false}
            }
          end
        end
      end
    end

    def wait_for_connection
      delay = Browser::Interval.new `window`, 1 do
        connected = !`#{Hyperloop.action_cable_consumer}.connection.disconnected`
        if connected
          Plugin.clear_cache!
          mutate.plugins Plugin::all
          mutate.loading false
          delay.abort
        end
      end
      delay.start
    end
  end
end
