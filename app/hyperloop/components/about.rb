class About < Hyperloop::Router::Component
  before_mount do
    # any initialization particularly of state variables goes here.
    # this will execute on server (prerendering) and client.
  end

  after_mount do
    # any client only post rendering initialization goes here.
    # i.e. start timers, HTTP requests, and low level jquery operations etc.
  end

  before_update do
    # called whenever a component will be re-rerendered
  end

  before_unmount do
    # cleanup any thing (i.e. timers) before component is destroyed
  end

  render do
    DIV(class_name: 'card') do
      DIV(class_name: 'card-content') do
        H1 { "About" }
        P {Hyperloop::Autoloader.history.to_json}
      end
    end
  end
end
