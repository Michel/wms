class MainNavigation < Hyperloop::Router::Component
  param :on_logout, type: Proc
  param :routes

  before_receive_props {|props| mutate.routes props[:routes]}

  render do
    NAV(role: 'navigation') do
      DIV(class: 'nav-wrapper container') do
        Link('/', class: 'brand-logo') {'WMS'}

        UL(class: 'right hide-on-med-and-down') do
          params.routes.each do |route|
            LI {NavLink(route.path) {route.name}}
          end
          LI {A {'Logout'}.on(:click) {on_logout}}
        end

        A(href: '#', class: 'sidenav-trigger', 'data-target': 'nav-mobile') do
          I(class: 'material-icons') {'menu'}
        end
      end


      UL(id: 'nav-mobile', class: 'sidenav') do
        params.routes.each do |route|
          LI {NavLink(route.path, class: 'sidenav-close') {route.name}}
        end
        LI {NavLink('/logout', class: 'sidenav-close') {'Logout'}.on(:click) {on_logout}}
      end
    end
  end

  def on_logout
    Session::LogoutOp.run.then {
      params.on_logout.call nil
    }
  end
end
