require 'models/user'
module Session
  class LoginPage < Hyperloop::Component
    include  React::IsomorphicHelpers

    state email: nil
    state password: nil
    state error: nil
    param :on_login, type: Proc

    before_mount do
    end

    after_mount do
      # any client only post rendering initialization goes here.
      # i.e. start timers, HTTP requests, and low level jquery operations etc.
    end

    before_update do
      # called whenever a component will be re-rerendered
    end

    before_unmount do
      # cleanup any thing (i.e. timers) before component is destroyed
    end

    def submit
      Session::LoginOp.run(email: state.email, password: state.password).then{ |userId|
        params.on_login User.find(userId)
      }.fail {|error|
        log error
        mutate.error error.message
      }
    end

    render do
      DIV(class_name: "valign-wrapper login-page") do
        DIV(class_name: "center-align row") do
          DIV(class_name: "card col s12 m4 push-m4 hoverable") do
            DIV(class_name: "card-content") do
              SPAN(class_name: "card-title") {"Login"}
              H4(class_name: "red-text") {state.error}
              FORM(class_name: 'material-form') {
                DIV(class_name: 'input-field') do
                  INPUT(id: "email", name: "email", defaultValue: state.email, autoComplete: 'off', data: {error: state.error}).on(:change) {|evt| mutate.email evt.target.value}
                  LABEL(htmlFor: "email", class_name: "#{"active" unless state.email.blank?}") {'Email'}
                end

                DIV(class_name: 'input-field') do
                  INPUT(name: "password", autoComplete: 'off', type: "password").on(:change) {|evt| mutate.password evt.target.value}
                  LABEL(htmlFor: "password", class_name: "#{"active" unless state.password.blank?}") {'Password'}
                end
              }.on(:submit) do |evt|
                evt.prevent_default
                submit
              end
            end
            DIV(class_name: "card-action") do
              A {"Login"}.on(:click) {submit}
            end
          end
        end
      end
    end
  end
end

