class Home < Hyperloop::Router::Component
  before_mount do
    @user = User.current
  end

  after_mount do
    # any client only post rendering initialization goes here.
    # i.e. start timers, HTTP requests, and low level jquery operations etc.
  end

  before_update do
    # called whenever a component will be re-rerendered
  end

  before_unmount do
    # cleanup any thing (i.e. timers) before component is destroyed
  end

  render do
    Loadable({ loadable: @user }) do
      DIV(class_name: 'card') do
        DIV(class_name: 'card-content') do
          H1 { "Home" }
        end
      end

      DIV(class_name: 'card') do
        DIV(class_name: 'card-content') do
          H2 {"Helloworld - #{@user&.email}"}
        end
      end
    end
  end
end
