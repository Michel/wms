module Admin
  module AdminOp
    def handle_exception(e, operation, params)
      if defined? ::Rails
        params.delete(:controller)
        ::Rails.logger.debug "\033[0;31;1mERROR: Hyperloop::ServerOp exception caught when running "\
                               "#{operation}\n\twith params \"#{params}\":\n\t #{e}\n\t#{e.backtrace.join "\n\t"}\033[0;30;21m"
      end
      {json: {error: e}, status: 500}
    end
    def self.handle_exception(e, operation, params)
      if defined? ::Rails
        params.delete(:controller)
        ::Rails.logger.debug "\033[0;31;1mERROR: Hyperloop::ServerOp exception caught when running "\
                               "#{operation}\n\twith params \"#{params}\":\n\t #{e}\n\t#{e.backtrace.join "\n\t"}\033[0;30;21m"
      end
      {json: {error: e}, status: 500}
    end
  end
end

ReactiveRecord::Operations::Base.extend ::Admin::AdminOp
Hyperloop::ServerOp.extend ::Admin::AdminOp
Hyperloop::ControllerOp.extend ::Admin::AdminOp