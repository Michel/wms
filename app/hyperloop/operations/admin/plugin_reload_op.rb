require_relative 'admin_op'
module Admin
  class PluginReloadOp < ::Hyperloop::ServerOp
    extend Admin::AdminOp

    param :acting_user

    step do
      ::PluginLoader.load_all
      true
    end
  end
end