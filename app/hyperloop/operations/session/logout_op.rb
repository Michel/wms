module Session
  class LogoutOp < Hyperloop::ControllerOp
    step {session[:current_user_id] = nil}
  end
end