module Session
  class LoginOp < Hyperloop::ControllerOp
    param :email
    inbound :password

    add_error(:email, :does_not_exist, 'that login does not exist') {!(@user = User.find_by_email(params.email))}
    add_error(:password, :is_incorrect, 'password is incorrect') {!@user&.authenticate(params.password)}

    step {session[:current_user_id] = @user&.id}
  end
end