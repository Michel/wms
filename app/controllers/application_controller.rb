class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def acting_user
    session[:current_user_id] && ::User.find(session[:current_user_id])
  end
end
