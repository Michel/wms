class PluginLoader
  class << self
    def load_all
      Dir["#{Rails.root}/plugins/*"].each do |plugin_dir|
        name = File.basename(plugin_dir)
        Rails.logger.info "LOADING #{name}"
        plugin = ::Plugin.find_or_create_by(name: name)
        if plugin.valid?
          new(plugin).load! reload_rails: false
          plugin.save!
        else
          plugin.update load_error: plugin.errors.full_messages.to_sentence
          Rails.logger.error plugin.errors.full_messages.to_sentence
        end
        Rails.logger.info "LOADING #{name} DONE - plugin valid? #{plugin.valid?}"
      end
      reload_rails!
    end

    def reload_rails!
      Rails.application.reloader.reload!
    end
  end
  attr_reader :plugin

  def initialize plugin
    @plugin = plugin
  end

  def load! opts = {}
    unload_constants
    add_to_loadpath
    run_migrations
    run_plugin_setup
    plugin.update load_error: "√", version: plugin.gemspec.version
    self.class.reload_rails! if opts.delete(:reload_rails) {true}
  rescue StandardError => e
    plugin.update load_error: e.message
  end

  def unload_constants
    ActiveSupport::Dependencies.remove_constant plugin.const_name
    Gem::Specification._clear_load_cache
  end

  def run_plugin_setup
    require_or_load("#{plugin.const_name.underscore}") if File.exist?("#{plugin.base_dir}/lib/#{plugin.const_name.underscore}.rb")
    const = begin
      plugin.const_name.constantize
    rescue NameError
      # ignored
    end
    const.setup if const && const.respond_to?(:setup)

    require_or_load("#{plugin.const_name.underscore}/engine") if File.exist?("#{plugin.base_dir}/lib/#{plugin.const_name.underscore}/engine.rb")
    engine_name = "#{plugin.const_name}::Engine"
    begin
      engine_name.constantize
    rescue NameError
      # ignored
    end
  end

  def run_migrations
    Plugin.transaction do
      migration_context = ActiveRecord::MigrationContext.new("#{plugin.base_dir}/db/migrate")
      migration_context.migrations.each {|migration| require_or_load(File.expand_path(migration.filename))}
      migration_context.migrate
    end
  end

  private

  def add_to_loadpath
    Rails.logger.info "Add plugin to load path: #{plugin.name}:#{plugin.version}"
    lib_dir = "#{plugin.base_dir}/lib"
    $LOAD_PATH << lib_dir unless $LOAD_PATH.include?(lib_dir)
    ActiveSupport::Dependencies.autoload_paths << lib_dir unless ActiveSupport::Dependencies.autoload_paths.include?(lib_dir)
    load plugin.gemspec_file
    Opal.append_path "#{plugin.base_dir}/app/hyperloop"
    Rails.application.config.assets.paths.unshift "#{plugin.base_dir}/app/hyperloop"
  end
end