class CreateRoutes < ActiveRecord::Migration[5.2]
  def change
    create_table :routes do |t|
      t.string :name
      t.string :path
      t.string :mount

      t.timestamps
    end
  end
end
