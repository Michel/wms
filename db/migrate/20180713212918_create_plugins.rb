class CreatePlugins < ActiveRecord::Migration[5.2]
  def change
    create_table :plugins do |t|
      t.string :name
      t.string :version
      t.text :load_error

      t.timestamps
    end
  end
end
