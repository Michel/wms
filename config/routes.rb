Rails.application.routes.draw do
  mount Hyperloop::Engine => '/hyperloop'

  root 'hyperloop#AppRouter'
  match '*all', to: 'hyperloop#AppRouter', via: [:get]
end
